from django.urls import path
from tasks.views import AddFormView, TaskListView, TaskUpdateView

urlpatterns = [
    path("create/", AddFormView.as_view(), name="create_task"),
    path("mine/", TaskListView.as_view(), name="show_my_tasks"),
    path("<int:pk>/complete/", TaskUpdateView.as_view(), name="complete_task"),
]
