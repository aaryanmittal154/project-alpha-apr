from django.shortcuts import redirect
from .forms import AddForm
from django.views.generic.edit import FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.list import ListView
from .models import Task
from django.views.generic.edit import UpdateView
from django.urls import reverse_lazy


class AddFormView(LoginRequiredMixin, FormView):
    template_name = "task_create.html"
    form_class = AddForm

    def form_valid(self, form):
        item = form.save()
        item.user_property = self.request.user
        item.save()
        return redirect("show_project", item.pk)


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "task_listview.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    template_name = "task_listview.html"
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
